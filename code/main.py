from fastapi import FastAPI

from router.user_router import router as user_router
from fastapi.middleware.cors import CORSMiddleware

tags_metadata = [{"name": "Login and Logout", "description": "authenticatiom endpoint"}]

app = FastAPI(title="Module Two API",
    description="This is an authentication module REST API using python and mysql",
    version="0.0.1",
    openapi_tags=tags_metadata
    )

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "http://127.0.0.1:8080"
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

app.include_router(user_router)