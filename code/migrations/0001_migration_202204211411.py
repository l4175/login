# auto-generated snapshot
from peewee import *
import datetime
import peewee


snapshot = Snapshot()


@snapshot.append
class TypeUser(peewee.Model):
    type_user = CharField(max_length=255)
    class Meta:
        table_name = "typeuser"


@snapshot.append
class User(peewee.Model):
    name = CharField(index=True, max_length=255)
    lastname = CharField(index=True, max_length=255)
    email = CharField(index=True, max_length=255, unique=True)
    password = CharField(max_length=255)
    id_type_users = snapshot.ForeignKeyField(index=True, model='typeuser', null=True)
    is_freelancer = BooleanField(default=False)
    Online = BooleanField(default=False)
    status = IntegerField(null=True)
    class Meta:
        table_name = "user"


