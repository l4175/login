
from datetime import datetime

import peewee

from utils.db import mysql_db



class TypeUser(peewee.Model):
    id = peewee.AutoField()
    type_user = peewee.CharField()
    

    class Meta:
        database = mysql_db

TypeUser.create_table()