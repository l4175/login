import peewee

from utils.db import mysql_db
from .todo_model import TypeUser

class User(peewee.Model):
    id = peewee.AutoField()
    name= peewee.CharField(index=True)
    lastname= peewee.CharField(index=True)
    email = peewee.CharField(unique=True, index=True)
    #username = peewee.CharField(unique=True, index=True)
    password = peewee.CharField()
    id_type_users = peewee.ForeignKeyField(TypeUser, to_field="id",default=None,null=True)
    is_freelancer = peewee.BooleanField(default=False)
    Online = peewee.BooleanField(default=False)
    status = peewee.IntegerField(null=True)

    class Meta:
        database = mysql_db

User.create_table()

