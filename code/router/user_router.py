from fastapi import APIRouter
from fastapi import Depends
from fastapi import status
from fastapi import Body
from fastapi.security import OAuth2PasswordRequestForm

from schema import user_schema
from service import user_service
from service import auth_service
from schema.token_schema import Token, TokenData
from schema.user_schema import UserBase
from service.auth_service import *

from utils.db import get_db


router = APIRouter(
    #prefix="/api/v1",
    tags=["users"]
)

@router.post(
    "/user",
    status_code=status.HTTP_201_CREATED,
    response_model=user_schema.UserBase,
    dependencies=[Depends(get_db)],
    summary="Create a new user"
)
def create_user(user: user_schema.UserBase = Body(...)):
    """
    ## Create a new user in the app
    ### Args
    The app can recive next fields into a JSON
    - name: Name of employee
    - lastname: Lastname of employee
    - email: A valid email 
    - password: Strong password for authentication
    - id_type_users: User Type, this data depent of User Type table 
    - is_freelancer: If user is freelancer, this data is boolean
    - Online: This data is boolean 
    - status: level of user, this data is integer 
    
    
   
    ### Returns
    - user: User info
    """
    return user_service.create_user(user)

@router.post(
    "/login",
    tags=["users"],
    response_model=Token
)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    """
    ## Login for access token
    ### Args
    The app can recive next fields by form data
    - email: Your email
    - password: Your password
    ### Returns
    - access token and token type
    """
    #print(UserModel.filter((UserModel.id_type_users == username)).first())
    access_token = auth_service.generate_token(form_data.username, form_data.password)
    return Token(access_token=access_token, token_type="bearer")





@router.post("/logout", response_model=TokenData, status_code=status.HTTP_200_OK)
async def logout(current_user: UserBase = Depends(get_current_active_user)):  # logout function to delete access token
    token_data = TokenData(username=current_user.email, expires=datetime.now())
    return token_data
    #return {'username':"User logout sucessful.", 'expires':'ok'}

@router.get("/users/me")
async def read_users_me(current_user: UserBase = Depends(get_current_active_user)):
    return current_user


