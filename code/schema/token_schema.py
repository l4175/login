from pydantic import BaseModel
from typing import Optional
from datetime import datetime, timedelta, timezone
from fastapi import Body



class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None
    #expires : datetime.now().isoformat()
    expires: datetime
    
    