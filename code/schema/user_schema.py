from unicodedata import name
from fastapi import Query
from pydantic import BaseModel
from pydantic import Field
from pydantic import EmailStr
from model.todo_model import TypeUser
from typing import Optional



class UserBase(BaseModel):
    id: Optional[int] 
    
    email: EmailStr = Field(
        ...,
        example="myemail@cosasdedevs.com"
    )
    password: str = Field(
        ...,
        min_length=8,
        max_length=64,
        example="strongpass"
    )
    name: str = Field(
        ...,
        min_length=3,
        max_length=50,
        example="MyTypicalUsername"
    )
    lastname: str = Field(
        ...,
        min_length=3,
        max_length=50,
        example="Rodriguez Ochoa"
    )
    id_type_users: Optional[str] 
    
    is_freelancer: bool = Field(
        ...,
        example=True
    )
    Online: bool = Field(
        ...,
        example=True
    )
    status: int = Field(
        ...,
        example="0"
    )
    


# class User(UserBase):
#     id: int = Field(
#         ...,
#         example="5"
#     )
    


class UserRegister(UserBase):
    password: str = Field(
        ...,
        min_length=8,
        max_length=64,
        example="strongpass"
    )