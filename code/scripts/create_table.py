from model.user_model import User
from model.todo_model import TypeUser

from utils.db import db

def create_tables():
    with db:
        db.create_tables([User, TypeUser])