from fastapi import HTTPException, status

from model.user_model import User as UserModel
from schema import user_schema
from service.auth_service import get_password_hash


def create_user(user: user_schema.UserBase):

    get_user = UserModel.filter((UserModel.email == user.email)).first() #| (UserModel.username == user.username)).first()
    if get_user:
        msg = "Email already registered"
        if get_user.name == user.name:
            msg = "name already registered"
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=msg
        )

    db_user = UserModel(
        name=user.name,
        lastname = user.lastname,
        email=user.email,
        password=get_password_hash(user.password),
        id_type_users = user.id_type_users,
        is_freelancer = user.is_freelancer,
        Online = user.Online,
        status = user.status
    )

    db_user.save()

    return user_schema.UserBase(
        id = db_user.id,
        name = db_user.name,
        email = db_user.email,
        id_type_users = db_user.id_type_users,
        is_freelancer = db_user.is_freelancer,
        Online = db_user.Online,
        status = db_user.status
    )